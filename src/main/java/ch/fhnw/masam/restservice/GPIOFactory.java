package ch.fhnw.masam.restservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GPIOFactory {

    public static final String GPIO_MODE_MOCK = "mock";
    public static final String GPIO_MODE_RASPBERRY = "raspberry";

    private static IGPIOUtil instance = null;
    private static String gpioMode;

    private static final Logger LOG = LoggerFactory.getLogger(GPIOFactory.class);

    public static void setGPIOMode(String gpioMode) {
        GPIOFactory.gpioMode = gpioMode;
    }

    public static IGPIOUtil getInstance() {

        if (instance == null) {
            if (gpioMode.equals(GPIO_MODE_RASPBERRY)) {
            	LOG.info("create raspberry pi util");
                instance = new RPGPIOUtil();
            } else if (gpioMode.equals(GPIO_MODE_MOCK)) {
            	LOG.info("create mock util");
                instance = new MockGPIOUtil();
            } else {
                // invalid mode
                LOG.error("invalid gpio mode: " + gpioMode);
            }
        }
        return instance;
    }
}
