package ch.fhnw.masam.restservice;

import org.junit.jupiter.api.*;

public class MockGPIOUtilTest {

    private MockGPIOUtil objToTest = null;

    @BeforeEach
    public void setUp() {
        objToTest = new MockGPIOUtil();
    }

    @Test
    public void getSetTest() throws Exception {
        Assertions.assertFalse(objToTest.getState(3));
        objToTest.setState(3, IGPIOUtil.STATE_HIGH);
        Assertions.assertTrue(objToTest.getState(3));
    }
    
}
